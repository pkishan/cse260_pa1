/* 
 *  A simple blocked implementation of matrix multiply
 *  Provided by Jim Demmel at UC Berkeley
 *  Modified by Scott B. Baden at UC San Diego to
 *    Enable user to select one problem size only via the -n option
 *    Support CBLAS interface
 */
#include <xmmintrin.h>
#include <malloc.h>
#include <stdlib.h>
const char* dgemm_desc = "Simple blocked dgemm.";

#if !defined(BLOCK_SIZE)
#define BLOCK_SIZE_2_1 64
#define BLOCK_SIZE_2_2 32 
#define BLOCK_SIZE_2_3 64 

#define BLOCK_SIZE_1_1 256
#define BLOCK_SIZE_1_2 128
#define BLOCK_SIZE_1_3 256
#endif

#define min(a,b) (((a)<(b))?(a):(b))

// /* 
// /* This auxiliary subroutine performs a smaller dgemm operation on L1 cache using register tiling of 2x2 size
//  *  C := C + A * B
//  * where C is M-by-N, A is M-by-K, and B is K-by-N. */

static void do_block_L1 (int lda, int M, int N, int K, double* restrict  A, double* restrict B, double* restrict C)
{
  /* For each row i of A */
  register __m128d c00,c10,a00,b00,a10,a01,b01,a11,ab0, b02, b03;
  int NN;
  if(N%2 == 0)
    NN = N;
  else
    NN = N + 1;
  double * restrict Be = memalign(2*sizeof(double), K*NN*sizeof(double));
  for(int k = 0; k < K-1; k+=2)
  {
    for(int j = 0; j < N-1; j+=2)
    {
      b00 = _mm_loadu_pd(&B[k*lda+j]);
      b01 = _mm_loadu_pd(&B[(k+1)*lda+j]);
      _mm_store_pd(&Be[k*NN+j] ,b00);
      _mm_store_pd(&Be[(k+1)*NN+j] ,b01);
     // _mm_prefetch((double*)&B[k*lda+j], _MM_HINT_NTA);
    }

    if(N%2!=0)
    {
      Be[k*NN+N-1] = B[k*lda+N-1];
      Be[(k+1)*NN+N-1] = B[(k+1)*lda+N-1];
    }

  }

  if(K%2!=0)
  {
    int k = K-1;
    for(int j = 0; j < N-1; j+=2)
    {
      b00 = _mm_loadu_pd(&B[k*lda+j]);
      _mm_store_pd(&Be[k*NN+j] ,b00);
    }

    if(N%2!=0)
    {
      Be[k*NN+N-1] = B[k*lda+N-1];
    }
  }

    for (int i = 0; i < M-1; i+=2)
  {
        /* For each column j of B */
        for (int j = 0; j < N-1; j+=2)
        {
            /* Compute C(i,j) */
          c00 = _mm_loadu_pd(&C[i*lda+j]);
          c10 = _mm_loadu_pd(&C[(i+1)*lda+j]);
        for (int k = 0; k < K-1; k+=2)
        {
          #ifdef TRANSPOSE
            cij += A[i*lda+k] * B[j*lda+k];
          #else
          a00 = _mm_load_pd1(&A[i*lda+k]);
          b00 = _mm_load_pd(&(Be[k*NN+j]));
          a10 = _mm_load_pd1(&A[(i+1)*lda+k]);
          a01 = _mm_load_pd1(&A[i*lda+k+1]);
          a11 = _mm_load_pd1(&A[(i+1)*lda+k+1]);
          b01 = _mm_load_pd(&(Be[(k+1)*NN+j]));
          ab0 = _mm_mul_pd(a00,b00);
          c00 = _mm_add_pd(c00,ab0);
          ab0 = _mm_mul_pd(a10,b00);
          c10 = _mm_add_pd(c10,ab0);
          ab0 = _mm_mul_pd(a01,b01);
          c00 = _mm_add_pd(c00,ab0);          
          ab0 = _mm_mul_pd(a11,b01);
          c10 = _mm_add_pd(c10,ab0);
          #endif
        }
         // The if part below is correct. 
        if(K%2 !=0)
        {
          int k = K-1;
          a00 = _mm_load_pd1(&A[i*lda+k]);
          b00 = _mm_load_pd(&Be[k*NN+j]);
          a10 = _mm_load_pd1(&A[(i+1)*lda+k]);
          ab0 = _mm_mul_pd(a00, b00);
          c00 = _mm_add_pd(c00, ab0);
          ab0 = _mm_mul_pd(a10, b00);
          c10 = _mm_add_pd(c10, ab0);
        }
          _mm_storeu_pd(&C[i*lda+j] ,c00);
          _mm_storeu_pd(&C[(i+1)*lda+j] ,c10);
      }
 
        if(N%2 !=0)
        {
            int L = N-1;
            double nc00 = C[i*lda + L];
            double nc10 = C[(i+1)*lda + L];
            for(int k = 0; k < K; k++)
            {
                #ifdef TRANSPOSE
                ncij += A[i*lda+k] * B[L*lda+k];
                #else
                nc00 += A[i*lda+k] * Be[k*NN+L];
                nc10 += A[(i+1)*lda+k] * Be[k*NN+L];

                #endif
                C[i*lda+L] = nc00;
                C[(i+1)*lda+L] = nc10;
            }
        }
        

  }
    if(M%2!=0)
{
  int i = M - 1;
  for (int j = 0; j < N-1; j+=2)
        {
           
          c00 = _mm_loadu_pd(&C[i*lda+j]);
        for (int k = 0; k < K-1; k+=2)
        {
          #ifdef TRANSPOSE
            cij += A[i*lda+k] * Be[j*NN+k];
          #else
          a00 = _mm_load_pd1(&A[i*lda+k]);
          b00 = _mm_load_pd(&(Be[k*NN+j]));
          a01 = _mm_load_pd1(&A[i*lda+k+1]);
          b01 = _mm_load_pd(&(Be[(k+1)*NN+j]));
          ab0 = _mm_mul_pd(a00,b00);
          c00 = _mm_add_pd(c00,ab0);
          ab0 = _mm_mul_pd(a01,b01);
          c00 = _mm_add_pd(c00,ab0);
          #endif
        }

        if(K%2 !=0)
        {
          int k = K-1;
          a00 = _mm_load_pd1(&A[i*lda+k]);
          b00 = _mm_load_pd(&Be[k*NN+j]);
          ab0 = _mm_mul_pd(a00,b00);
          c00 = _mm_add_pd(c00, ab0);
        }
          _mm_storeu_pd(&C[i*lda+j] ,c00);

      }

        if(N%2 !=0)
        {
      int L = N-1;
            double ncij = C[i*lda + L];
            for(int k = 0; k < K; k++)
            {
                #ifdef TRANSPOSE
                ncij += A[i*lda+k] * B[L*lda+k];
                #else
                ncij += A[i*lda+k] * Be[k*NN+L];
                #endif
                C[i*lda+L] = ncij;

            }
        }
}
free(Be);
}

/* This auxiliary subroutine performs a smaller dgemm operation on L2 cache which laods blocks to push to L1 cache
 *  C := C + A * B
 * where C is M-by-N, A is M-by-K, and B is K-by-N. */
static void do_block_L2 (int lda, int M, int N, int K, double* restrict A, double* restrict B, double* restrict C)
{
  /* For each row i of A */
  for (int i = 0; i < M; i+=BLOCK_SIZE_2_1)
    /* For each column j of B */ 
    for (int j = 0; j < N; j+=BLOCK_SIZE_2_2) 
    {
      /* Compute C(i,j) */
      for (int k = 0; k < K; k+=BLOCK_SIZE_2_3)
      {
        /* Correct block dimensions if block "goes off edge of" the matrix */
        int X = min (BLOCK_SIZE_2_1, M-i);
        int Y = min (BLOCK_SIZE_2_2, N-j);
        int Z = min (BLOCK_SIZE_2_3, K-k);
        
        /* Perform individual block dgemm */
        do_block_L1(lda, X, Y, Z, A + i*lda + k, B + k*lda + j, C + i*lda + j);

      }
    }
}

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in row-major order
 * On exit, A and B maintain their input values. */  
void square_dgemm (int lda, double* restrict A, double* restrict B, double* restrict C)
{
  /* For each block-row of A */ 
  for (int i = 0; i < lda; i += BLOCK_SIZE_1_1)
    /* For each block-column of B */
    for (int j = 0; j < lda; j += BLOCK_SIZE_1_2)
      /* Accumulate block dgemms into block of C */
      for (int k = 0; k < lda; k += BLOCK_SIZE_1_3)
      {
  /* Correct block dimensions if block "goes off edge of" the matrix */
  int M = min (BLOCK_SIZE_1_1, lda-i);
  int N = min (BLOCK_SIZE_1_2, lda-j);
  int K = min (BLOCK_SIZE_1_3, lda-k);

  /* Perform individual block dgemm */
  do_block_L2(lda, M, N, K, A + i*lda + k, B + k*lda + j, C + i*lda + j);
      }
#ifdef TRANSPOSE
  for (int i = 0; i < lda; ++i)
    for (int j = i+1; j < lda; ++j) {
        double t = B[i*lda+j];
        B[i*lda+j] = B[j*lda+i];
        B[j*lda+i] = t;
  }
#endif
}
